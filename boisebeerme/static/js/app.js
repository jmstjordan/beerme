'use strict';
var $app = angular.module('beerme', ['services','controller', 'smart-table', 'chart.js']);

$app.config(['$httpProvider', function($httpProvider){
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);
$app.config(function($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
});
