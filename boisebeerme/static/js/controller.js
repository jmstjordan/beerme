'use strict';

var controller = angular.module('controller', []);
controller.controller('controller', function($scope, api, $http) {
	$scope.name = "Jan-Michael";

	api.tapLists.get().$promise.then(function(data){
    	$scope.locations = data;

    	var currentOfferings = [];

    	var locationLabels = [];
        var beerCounts = [];

    	angular.forEach(data, function(location){

    	    var mostRecentOfferingDate = '';

    	    // these next two lines are for our chart
    	    var locationBeerCount = 0;
    	    locationLabels.push(location.name);
            var stopCount = 1;
            angular.forEach(location.offerings, function(offering){
                if(mostRecentOfferingDate == ''){
                    mostRecentOfferingDate = offering.offering_date;
                }
                if(mostRecentOfferingDate == offering.offering_date){
                    offering["locationName"] = location.name;
                    currentOfferings.push(offering);
                    locationBeerCount++;
                }
                stopCount++;
                if(stopCount > 135)
                {
                    break;
                }
            })
            beerCounts.push(locationBeerCount);
        })
        $scope.offerings = currentOfferings;

        // Populate bar chart.js

        data = {
            type: 'bar',
            data: {
                labels: locationLabels,
                datasets: [{
                    label: '# of Taps',
                    data: beerCounts,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        };

        $scope.barChartHeader = 'Taps per Location';
        var ctx = document.getElementById("barChart").getContext("2d");
        var barChart = new Chart(ctx, data);

    }); //end taplist api call

    api.beerStyles.get().$promise.then(function(data){

        var pieLabels = [];
        var localPieData = [];

        angular.forEach(data, function(beerStyleObject){
           pieLabels.push(beerStyleObject.beer_style);
           localPieData.push(beerStyleObject.beer_style_count);
        })

        // Populate pie data
        var pieData = {
            labels: pieLabels,
            datasets: [
                {
                    data: localPieData,
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#AF6384",
                        "#99A2EB",
                        "#CFCE56",
                        "#BFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#AF6384",
                        "#99A2EB",
                        "#CFCE56",
                        "#BFCE56"
                    ]
                }
            ]
        };
        $scope.pieChartHeader = '% of Current Beers Types';
        var pie_ctx = document.getElementById("pieChart").getContext("2d");
        var myPieChart = new Chart(pie_ctx,{
            type: 'pie',
            data: pieData
        });
    }); //end beerstyle api call
});

