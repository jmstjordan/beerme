'use strict';
var services = angular.module('services', ['ngResource']);
services.factory('api', function($resource){
    // defining the endpoints. Note we escape url trailing dashes: Angular
    // strips unescaped trailing slashes. Problem as Django redirects urls
    // not ending in slashes to url that ends in slash for SEO reasons, unless
    // we tell Django not to [3]. This is a problem as the POST data cannot
    // be sent with the redirect. So we want Angular to not strip the slashes!
    return {
        tapLists: $resource('/api/taps/', {}, {
        	get: {method: 'GET', isArray:true}
        }),
        beerStyles: $resource('/api/beerstyles/', {}, {
        	get: {method: 'GET', isArray:true}
        })
    };
});
