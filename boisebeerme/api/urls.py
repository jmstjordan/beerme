from django.conf.urls import url
from boisebeerme.api import views

urlpatterns = [
    url(r'^taps/$',views.TapLists.as_view(),name='taps'),
	url(r'^beerstyles/$', views.BeerStyles.as_view(), name='beerstyles'),
]
