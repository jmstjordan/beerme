from rest_framework import serializers
from boisebeerme.api.models import Location, Offering, Beer, Brewery
from datetime import date, timedelta

class FilteredOfferingSerializer(serializers.ListSerializer):

	def to_representation(self, data):
		return super(FilteredOfferingSerializer, self).to_representation(data)


class BeerStylesSerializer(serializers.Serializer):
	beer_style = serializers.CharField(max_length=200)
	beer_style_count = serializers.IntegerField()


class BrewerySerializer(serializers.ModelSerializer):
	class Meta:
		model = Brewery
		fields = ('name', 'brewery_id')

class BeerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Beer
		fields = ('name', 'brewery', 'description', 'beer_id', 'brewery', 'style', 'category_name', 'ibu', 'abv', 'scraped_name')

class OfferingSerializer(serializers.ModelSerializer):
	beer = BeerSerializer()
	class Meta:
		model = Offering
		list_serializer_class = FilteredOfferingSerializer
		fields = ('offering_date', 'beer')

class LocationSerializer(serializers.ModelSerializer):
	offerings = OfferingSerializer(many=True)
	def create(self, validated_data):
		offerings = validated_data.pop('offerings')
		location = Location.objects.create(**validated_data)
		for offering in offerings:
			beer_data = offering.pop('beer')
			beer = self.create_or_update_beer(beer_data)
			self.get_or_create_offering(beer, location)
		return location

	def update(self, instance, validated_data):
		"""
		Update and return an existing `Location` instance, given the validated data.
		"""
		print(self.context)
		offerings = validated_data.pop('offerings')
		instance.name = validated_data.get('name', instance.name)
		instance.address = validated_data.get('address', instance.address)
		instance.phone_number = validated_data.get('phone_number', instance.phone_number)
		instance.closed_location = validated_data.get('closed_location', instance.closed_location)
		instance.save()
		for offering in offerings:
			beer_data = offering.pop('beer')
			beer = self.create_or_update_beer(beer_data)
			self.get_or_create_offering(beer, instance)
		return instance

	def get_or_create_offering(self, beer, location):
		print(location)
		today = date.today()

		recent_offerings = Offering.objects.filter(offering_date=today,beer=beer,location=location)
		if len(recent_offerings) == 0:
			Offering.objects.create(beer=beer,location=location)

	def create_or_update_beer(self, beer_data):
		beers = Beer.objects.filter(name=beer_data['name'])
		if len(beers) == 0:
			return Beer.objects.create(**beer_data)
		else:
			beer = beers[0]
			beer.name = beer_data.get('name', beer.name)
			beer.brewery = beer_data.get('brewery', beer.brewery)
			beer.description = beer_data.get('description', beer.description)
			beer.bdb_id = beer_data.get('bdb_id', beer.bdb_id)
			beer.style = beer_data.get('style', beer.style)
			beer.ibu = beer_data.get('ibu', beer.ibu)
			beer.abv = beer_data.get('abv', beer.abv)
			beer.srm = beer_data.get('srm', beer.srm)
			beer.color = beer_data.get('color', beer.color)
			beer.save()
			return beer
	
	def get_serializer_context(self):
		print(self.kwargs['offerings'])
		return {"offering_list": self.kwargs['offerings']}

	class Meta:
		model = Location
		fields = ('id', 'name', 'site', 'address', 'phone_number', 'closed_location', 'offerings')
