import difflib
from boisebeerme.api.models import *

# determines how accurate you want the search to be for beers -> beers in bdb
# 0 - 1 scale, 1 being identical, 0 being not very identical
ACCURACY = 0.75

KEYWORDS_TO_REMOVE = [
	#breweries
	'odell'
	,'woodland empire'
	,'sockeye'
	,'payette'
	,'boise brewing'
	,'deschutes'
	,'firestone walker'
	,'goose island'
	,'new belgium'
	,'10 barrel'
	,'green flash'
	,'anderson valley'
	,'rogue'
	,'weihenstephaner'
	,'angry orchard'
	,'lagunitas'

	#styles
	# make sure these are ordered from larger length.... to shortest
	,'russian imperial'
	,'amber ale'
	,'winter ale'
	,'golden ale'
	,'blonde ale'
	,'wild ale'
	,'belgian quad'
	,'pilsner'
	,'hefeweizen'
	,'pale ale'
	,'porter'
	,'imperial stout'
	,'cider'
	,'stout'
	,'amber'
	,'nitro'
	,'ipa'
	,'ale'

	#other
	,'>'
	,'/'
	,':'
	,'-'
]


def check_locally_for_beer(orphan_beer_name):
	"""
	Takes in a beer name, then checks locally to see if a similar name
	exists locally, if it does, we return the beer in which exists
	:param beer_name:
	:return: None if the beer doesn't exist already,
	"""

	# Get all list of beers that are not orphans
	# We can filter this down when we get more beers for efficiency, now I am just getting it running
	existing_beers = Beer.objects.filter(beer_id__isnull=False)

	existing_beer_names = []
	existing_beer_name_to_beer = {}
	for existing_beer in existing_beers:
		existing_beer_names.append(existing_beer.name.lower())
		existing_beer_name_to_beer[existing_beer.name.lower()] = existing_beer

	orphan_beer_name_to_search = '' + orphan_beer_name.lower()

	# removing the brewery name from the beer name that was scraped,
	# strictly for searching purposes
	for keyword in KEYWORDS_TO_REMOVE:
		if keyword in orphan_beer_name_to_search:
			orphan_beer_name_to_search = orphan_beer_name_to_search.replace(keyword, '')

	best_matches = difflib.get_close_matches(orphan_beer_name_to_search, existing_beer_names, 1, ACCURACY)

	if len(best_matches) > 0:
		# we have found at least one match
		with open("passed_beers.txt", "a") as myfile:
			myfile.write("\n{orphan_beer} -> {best_match}"
											 .format(orphan_beer=orphan_beer_name, best_match=best_matches))
		print("Best match for {orphan_beer} was {best_match}"
											 .format(orphan_beer=orphan_beer_name, best_match=best_matches))
		return existing_beer_name_to_beer[best_matches[0]]
	else:
		with open("no_match_beers.txt", "a") as myfile:
			myfile.write("\n{orphan_beer}"
						 .format(orphan_beer=orphan_beer_name, best_match=best_matches))
		print("{orphan_beer}".format(orphan_beer=orphan_beer_name))
		return None

		# Somehow verify that it is close enough?
