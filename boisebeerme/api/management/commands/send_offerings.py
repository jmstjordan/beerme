from django.core.management.base import BaseCommand, CommandError
from boisebeerme.api.models import *
from boisebeerme.account.models import *
from django.core.mail import send_mail, EmailMultiAlternatives
from datetime import date, timedelta



class Command(BaseCommand):
	help = 'Sends offerings to all subscribers'

	FROM = 'jmstjordan@gmail.com'
	REPLY_TO = 'jmstjordan@gmail.com'
	SUBJECT = 'Weekly Tap List for {today}'.format(today=date.today())


	def handle(self, *args, **options):

		users = User.objects.all()

		for user in users:
			if user.subscriber.is_subscriber:
				locations = self.get_locations_by_subscriber(user.subscriber)

				html = ''
				count = 0
				for location in locations:
					html += self.build_html(location, '', count)
					count += 1
				if html == '':
					html = 'No offerings available'
				msg = EmailMultiAlternatives(subject=self.SUBJECT, from_email=self.FROM, bcc=[user.email])
				msg.attach_alternative(html, "text/html")
				msg.send()

		self.stdout.write(self.style.SUCCESS('Successfully sent out latest beer lists'))


	def build_html(self, location, html, count):
		"""
		Builds html email string from json beers
		:return:
		"""

		offerings = location.offerings.all()
		latest_date = None
		if not offerings:
			return ''
		latest_date = offerings.latest('offering_date').offering_date
		current_offerings = offerings.filter(offering_date=latest_date)

		if count != 0:
			html += '</table>'

		html += '<h4 style="color: blue">{name}</h4>'.format(name=location.name)
		html += '<table style="width:800px; table-layout:fixed">'
		html += '<tr><th align="left" style="width:350px">Beer</th><th align="left" style="width:200px">Brewery</th> ' \
				'<th align="left" style="width:150px">Style</th> <th align="left" style="width:50px">ABV</th> <th align="left" style="width:50px">IBU</th></tr>'

		for offering in current_offerings:
			beer_name = None
			brewery_name = None
			abv = None
			ibu = None
			style = None
			try:
				beer_name = offering.beer.name.title()
			except AttributeError:
				pass
			try:
				brewery_name = offering.beer.brewery.name
			except AttributeError:
				pass
			try:
				abv = offering.beer.abv
			except AttributeError:
				pass
			try:
				ibu = offering.beer.ibu
			except AttributeError:
				pass
			try:
				style = offering.beer.style
			except AttributeError:
				pass
			html += '<tr><td>{beer_name}</td><td>{brewery_name}</td> <td>{style}</td> <td>{abv}</td> <td>{ibu}</td></tr>'\
				.format(beer_name=beer_name, brewery_name=brewery_name, style=style, abv=abv, ibu=ibu)
		return html


	def get_locations_by_subscriber(self, subscriber):
		"""
		Takes in a subscriber, and returns a list of locations for which
		that subscriber wants taplists from
		:return:
		"""
		locations = []
		if subscriber.FredMeyerEB:
			locations.append(Location.objects.get(name='Fred Meyer - East Bench'))
		if subscriber.Payette:
			locations.append(Location.objects.get(name='Payette'))
		if subscriber.BoiseBrewing:
			locations.append(Location.objects.get(name='Boise Brewing'))
		if subscriber.OldChicagoDT:
			locations.append(Location.objects.get(name='Old Chicago - Downtown'))
		if subscriber.BrewersHaven:
			locations.append(Location.objects.get(name='Brewers Haven'))
		if subscriber.BitterCreek:
			locations.append(Location.objects.get(name='BitterCreek'))
		if subscriber.Bier30:
			locations.append(Location.objects.get(name='Bier30'))
		if subscriber.YardHouse:
			locations.append(Location.objects.get(name='Yard House'))

		return locations
