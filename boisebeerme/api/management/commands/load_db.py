from django.core.management.base import BaseCommand, CommandError
from boisebeerme.api.models import *
from datetime import date, timedelta
import boisebeerme.api.management.commands.__beermedao as local_dao

class Command(BaseCommand):
	help = 'Loads the Database with beer results'

	def handle(self, *args, **options):

		breweries = self.read_breweries_from_file()
		self.load_db(breweries)
		self.stdout.write(self.style.SUCCESS('Successfully loaded db'))


	def load_db(self, breweries):
		"""
		Takes in a dictionary of breweries and loads the DB with them
		:return:
		"""
		for location, beers in breweries.items():
			loc = self.get_location(location)
			self.stdout.write(self.style.SUCCESS('Scraped loc "%s"' % loc.name))
			for beer_name in beers:
				#get beer, we are actually simply saving off the scraped data
				#here, the only reason we search is if we have already scraped it
				#these rows are going to be updated with a future command that is
				#a more robust search, for now we just don't want two copies
				#of old data
				beer = self.get_beer(beer_name)
				self.get_or_create_offering(beer, loc)
				#self.stdout.write(self.style.SUCCESS('Successfully checked or created offering for "%s"' % beer.name))


	def read_breweries_from_file(self):
		"""
		Reads results from scraping, we won't need this when droplet is functional with selenium.
		Returns a dictionary that we can use for inputting scraped results in db
		:return:
		"""
		breweries = {}

		for location in Location.objects.all():
			print(location.name)
			with open('/home/jm/repos/beerme/boisebeerme/raw_offerings/{file}.txt'.format(file=location.name), 'r') as f:
				lines = f.readlines()
				lines = [line.rstrip() for line in lines]
				breweries[location.name] = lines

		return breweries


	def get_location(self, name):
		try:
			location = Location.objects.get(name=name)
		except Location.DoesNotExist:
			location = Location(name=name)
			location.save()
		return location

	def get_beer(self,beer_name):
		"""
		Checks to see if we already have a valid beer_id stored for this beer name.
		This is tricky because we dont have the second PK, the brewery name, up front
		with the scrape, so we need to get thru BDB. But we don't want to do it query
		them for every beer if we dont have to, but this could be a problem later on
		since we are not defining both keys with this search.

		:param beer_name:
		:return: clean beer name if it already exists, or else we create a new one
		for now and update later
		"""

		beers = Beer.objects.filter(name=beer_name)
		if len(beers) > 0:
			#basically we have already scraped it, so we just return that data
			#in the case that we happen to have the same scraped name as the bdb
			#name, we are just returning the correct one so it works out fine
			return beers[0]
		else:
			existing_beer = local_dao.check_locally_for_beer(beer_name)
			if existing_beer:
				existing_beer.scraped_name = beer_name
				existing_beer.save()
				return existing_beer
			beer = Beer(name=beer_name, scraped_name=beer_name)
			beer.save()
			return beer


	def get_or_create_offering(self, beer, location):
		today = date.today()
		offerings = Offering.objects.filter(offering_date=today,beer=beer,location=location)
		if len(offerings) == 0:
			offering = Offering(offering_date=today, beer=beer,location=location)
			offering.save()
