from django.core.management.base import BaseCommand, CommandError
from boisebeerme.api.models import *
import boisebeerme.brewerydb.dao as dao

class Command(BaseCommand):
	help = 'Pulls local beers and breweries information through Brewery DB'

	def handle(self, *args, **options):

		brewery_ids_to_names = {}
		# until I implement a webhook, we dont really need this after this initial db load
		brewery_ids_to_names = dao.get_boise_brewery_ids_by_radius()
		brewery_ids_to_names = dao.get_other_brewery_ids(brewery_ids_to_names)

		if brewery_ids_to_names is None:
			self.stdout.write(self.style.SUCCESS("No brewery Ids found"))

		for brewery_id, brewery_name in brewery_ids_to_names.items():
			try:
				brewery = Brewery.objects.get(brewery_id=brewery_id)
			except Brewery.DoesNotExist:
				print('brewery id = {bid}\nbrew name = {bname}'.format(bid=brewery_id, bname=brewery_name))
				brewery = Brewery(name=brewery_name, brewery_id=brewery_id)
				brewery.save()
			self.add_or_update_beers(dao.get_beers_from_brewery(brewery_id), brewery)
		self.stdout.write(self.style.SUCCESS("Successfully added or updated local beers"))

	def add_or_update_beers(self, beers_json, brewery):
		"""
		Takes in some json array of beers, and saves/updates the beers from bdb,
		should basically always be trying to update missing beers after the first
		run
		:param beers_json:
		:return:
		"""
		if beers_json is None:
			print('No beers to update for breweryid: {bid}'.format(bid=brewery.brewery_id))
			return
		for beer in beers_json:
			status = None
			try:
				status = beer['status']
			except KeyError:
				pass
			if status == 'verified':
				try:
					new_beer = Beer.objects.get(beer_id=beer['id'])
					# keep this return in if we dont want to update beers we already have, this will speed up
					# our pull_breweries run time drastically, but we will miss out on updated beer info.
					return

				except Beer.DoesNotExist:
					new_beer = Beer()
				try:
					new_beer.name = beer['name']
				except KeyError:
					pass
				try:
					new_beer.beer_id = beer['id']
				except KeyError:
					pass
				try:
					new_beer.abv = beer['abv']
				except KeyError:
					pass
				try:
					new_beer.ibu = beer['ibu']
				except KeyError:
					pass
				try:
					new_beer.style = beer['style']['name']
				except KeyError:
					pass
				try:
					new_beer.category_name = beer['style']['category']['name']
				except KeyError:
					pass
				new_beer.brewery = brewery
				print('From {brewery} adding {beer}'.format(brewery=brewery.name, beer=new_beer.name))
				new_beer.save()


	def count_active_beer_ids(self):
		total = len(Beer.objects.all())
		total_verified = len(Beer.objects.filter(bdb_id__isnull=False))
		self.stdout.write(self.style.SUCCESS('{total_verified} / {total} beers have an id'.
											 format(total=total, total_verified=total_verified)))
