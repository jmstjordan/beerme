from django.core.management.base import BaseCommand, CommandError
from boisebeerme.api.models import *
import boisebeerme.brewerydb.dao as dao
import boisebeerme.api.management.commands.__beermedao as local_dao

class Command(BaseCommand):
	help = 'Attempts to update orphan beers locally first, and then through bdb'


	def handle(self, *args, **options):

		orphan_beers = Beer.objects.all().filter(beer_id__isnull=True)

		num_orphans_start = len(orphan_beers)
		num_found = 0
		num_not_found = 0

		for orphan_beer in orphan_beers:
			existing_beer = local_dao.check_locally_for_beer(orphan_beer.name)
			if existing_beer is not None:
				num_found += 1
				self.combine_orphan_and_existing(orphan_beer, existing_beer)
			else:
				num_not_found += 1

		self.stdout.write(self.style.SUCCESS('{true} / {total} beers were matched'.
											 format(total=num_not_found+num_found, true=num_found)))

		# Future development will attempt to sanitize the data and search bdb, but
		# for now its too risky, I dont want to have bad data, we will continue to
		# add breweries to our 'pull_breweries' command, which will pull down more
		# and more beers, and hopefully that will remove orphans

		updated = num_orphans_start - Beer.objects.filter(beer_id__isnull=True).count()

		self.stdout.write(self.style.SUCCESS("Successfully updated {num} beers\n".format(num=updated)))


	def combine_orphan_and_existing(self, orphan_beer, existing_beer):
		"""
		Since we already created our offering with scrape_sites, I think
		it is easier if we remove the beer that already exists, and update
		the orphan. If we wait and create our offerings after this command,
		we lose loc. name in memory, and have to break the offerings. No fun.
		:param beer_name:
		:return:
		"""

		beer_id = existing_beer.beer_id

		try:
			orphan_beer.name = existing_beer.name
		except KeyError:
			pass
		try:
			orphan_beer.abv = existing_beer.abv
		except KeyError:
			pass
		try:
			orphan_beer.ibu = existing_beer.ibu
		except KeyError:
			pass
		try:
			orphan_beer.style = existing_beer.style
		except KeyError:
			pass
		orphan_beer.brewery = existing_beer.brewery

		# so if the existing beer has a scraped name, we know that
		# it was originally a scraped offering, not a pulled beer from bdb,
		# so we don't want to delete it, because it is associated with an offering

		# note the only way we make it in here is if we have pulled a new beer and
		# we are updating old orphans, shouldn't happen too often
		if not existing_beer.scraped_name:
			if Beer.objects.filter(beer_id=beer_id).delete():
				self.stdout.write(self.style.SUCCESS("Successfully deleted {beer}\n".format(beer=orphan_beer.name)))

		orphan_beer.beer_id = beer_id
		orphan_beer.save()


	def update_data_for_beers(self):
		"""
		Updates all beers in our db without an active beer id thru bdb,
		once this beer id is grabbed, we save it, and we can use then
		use that id to get brewery and beer info for this beer
		"""

		# when we initially save off our beers, this is the default
		# until we have a chance to clean up

		beers = Beer.objects.filter(bdb_id__isnull=True)

		for beer in beers:
			beer_id = dao.search_for_id(beer.name)
			if beer_id != 'NO_ID':
				self.stdout.write(self.style.SUCCESS('Successfully found ID: {id}'.format(id=beer_id)))
				self.update_stats(beer_id, beer.name)
			else:
				self.stdout.write(self.style.SUCCESS('Failed to find ID: {id}'.format(id=beer_id)))


	def update_stats(self, beer_id, old_beer_name):
		"""
		Takes in the beer id, runs it thru bdb and
		updates our Beer model with appopriate stats

		:param beer_id:
		:return:
		"""

		dict_stats = dao.get_misc_beer_stats(beer_id)

		print('MY DICT-----{0}'.format(dict_stats))

		try:
			beer = Beer.objects.get(name=old_beer_name)
		except Beer.DoesNotExist:
			return

		try:
			beer.bdb_id = dict_stats['bdb_id']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: bdb_id'))

		try:
			beer.name = dict_stats['name']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: Name'))

		try:
			beer.brewery = dict_stats['brewery']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: brewery'))

		try:
			beer.abv = dict_stats['abv']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: abv'))

		try:
			beer.ibu = dict_stats['ibu']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: ibu'))

		try:
			beer.description = dict_stats['description']
		except KeyError:
			self.stdout.write(self.style.SUCCESS('Failed to populate field: description'))

		# too scared to to do a try catch against an IntegrityError, I want to flush
		# out bugs that I dont know yet, so for now we will do a simple check to
		# see if the beer pk already exists
		try:
			beer = Beer.objects.get(name=beer.name, brewery=beer.brewery)
		except Beer.DoesNotExist:
			beer.save()

