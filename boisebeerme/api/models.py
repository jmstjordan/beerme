from django.db import models
from django.core.validators import RegexValidator


class Location(models.Model):
	"""
	A location that has a taplist
	"""
	name = models.CharField(max_length=250, unique=True, default="UNKNOWN")
	site = models.URLField(null=True, blank=True)
	address = models.TextField(null=True, blank=True)
	phone_regex = RegexValidator(regex=r'^\([0-9]{3}\)\s[0-9]{3}-[0-9]{4}$', message="Phone number must be entered in the format: '(999) 999-9999'")
	phone_number = models.CharField(null=True, blank=True, validators=[phone_regex], max_length=15)
	closed_location = models.BooleanField(default=False)
	automated = models.BooleanField(default=True)


class Brewery(models.Model):
	"""
	Breweries for information purposes so we can easily map beers through BDB
	"""
	name = models.CharField(max_length=250, unique=True)

	# This can't be part of the pk because it relies on being a part of brewerydb
	brewery_id = models.CharField(max_length=25, null=True)


class Beer(models.Model):
	"""
	this model is our beer, it could be either a scraped empty model, or a beer
	that was mapped to a beer in brewerydb.com
	"""
	name = models.CharField(max_length=250)
	description = models.TextField(null=True, blank=True)
	scraped_name = models.TextField(null=True, blank=True)

	# These two ideas are associated with the api of brewerydb.com
	# These can't be part of the pk because it relies on being a part of brewerydb
	beer_id = models.CharField(max_length=25, null=True)
	brewery = models.ForeignKey(Brewery, null=True)
	style = models.CharField(max_length=250, null=True)
	category_name = models.CharField(max_length=250, null=True)

	ibu = models.FloatField(null=True, blank=True)
	abv = models.FloatField(null=True, blank=True)


class Offering(models.Model):
	"""
	Represents an instance of when a beer was offered at a given location
	"""
	offering_date = models.DateField(auto_now_add=True)
	beer = models.ForeignKey(Beer, on_delete=models.CASCADE)
	location = models.ForeignKey(Location, related_name='offerings')

	class Meta:
		unique_together = (("offering_date", "beer", "location"))
		ordering = ['-offering_date']