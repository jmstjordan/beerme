from django.core.mail import send_mail, BadHeaderError
from django.http import JsonResponse, Http404
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from boisebeerme.api.models import Location, Offering, Beer, Brewery
from boisebeerme.api.serializers import OfferingSerializer, LocationSerializer, BeerSerializer, BrewerySerializer, BeerStylesSerializer
import math

class TapLists(APIView):
	permission_classes=[AllowAny]
	def get_location(self, pk):
		try:
			return Location.objects.get(pk=pk)
		except Location.DoesNotExist:
			raise Http404

	def get(self, request, format=None):
		locations = Location.objects.all()
		serializer = LocationSerializer(locations, many=True, context={'current' : True})
		return Response(serializer.data)

	def post(self, request, format=None):
		serializer = LocationSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def put(self, request, format=None):
		id = request.data['id']
		location = self.get_location(id)
		serializer = LocationSerializer(location, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BeerStyles(APIView):
	permission_classes=[AllowAny]
	def get(self, request, format=None):

		beers_styles = {
			"IPA": 0,
			"Amber/Hefeweizen": 0,
			"Belgian": 0,
			"Porter/Stout": 0,
			"Light Ale": 0,
			"Cider/Kombucha": 0
		}

		locations = Location.objects.all()
		totalOfferings = 0
		for location in locations:
			offerings = location.offerings.all()
			latest_date = None
			if not offerings:
				continue
			latest_date = offerings.latest('offering_date').offering_date
			current_offerings = offerings.filter(offering_date=latest_date)

			for offering in current_offerings:
				local_style = self.get_beer_style(offering)
				if local_style != "Other":
					beers_styles[local_style] += 1
					totalOfferings += 1

		beer_style_objs = []
		for style, count in beers_styles.items():
			beer_style_objs.append(BeerStyle(beer_style=style, beer_style_count=float(count/totalOfferings)*100))
			print("{0} {1}".format(style, math.ceil(float(count/totalOfferings)*100)))
		serializer = BeerStylesSerializer(beer_style_objs, many=True)
		return Response(serializer.data)

	def get_beer_style(self, offering):
		"""
		Helper function that takes in an offering, and returns
		the style associated with that offering
		:param offering:
		:return:
		"""

		beer = offering.beer
		beer_name = None
		if beer.category_name is not None:
			beer_name = beer.category_name.lower()
		else:
			beer_name = beer.name.lower()

		# print(beer_name)

		if "ipa" in beer_name or "pale" in beer_name:
			return "IPA"
		if "amber" in beer_name or "hefe" in beer_name:
			return "Amber/Hefeweizen"
		if "belgian" in beer_name:
			return "Belgian"
		if "porter" in beer_name or "stout" in beer_name:
			return "Porter/Stout"
		if "lager" in beer_name or "light" in beer_name or "blonde" in beer_name:
			return "Light Ale"
		if "cider" in beer_name or "kombucha" in beer_name:
			return "Cider/Kombucha"
		return "Other"


class BeerStyle(object):
	"""
	Custome object that we are sending for our Beer Style count rest api call
	"""
	def __init__(self,  beer_style, beer_style_count):
		self.beer_style = beer_style
		self.beer_style_count = beer_style_count


