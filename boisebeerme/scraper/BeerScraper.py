from selenium import webdriver
from selenium.webdriver.common.by import By
from xvfbwrapper import Xvfb
import logging
import sys

chrome_driver_path = "/usr/bin/chromedriver"
logging.basicConfig(filename='beer.log', level=logging.DEBUG)


def scrape():
    """
    Default scraper function, essentially passes in a location and
    will scrape that location and do all of our cleanup
    :return:
    """
    xvfb = Xvfb()
    xvfb.start()

    location = sys.argv[1]
    beers = get_beers(location)
    print(beers)
    xvfb.stop()
    if not beers:
        return   
    with open('/home/jm/repos/beerme/boisebeerme/raw_offerings/{file}.txt'.format(file=location), 'w') as f:
        for beer in beers:
            f.write(beer + '\n')


def get_driver(link):
    """
    Helper to get link, as I can't pass the driver through
    because python
    :return:
    """
    driver = webdriver.Chrome(chrome_driver_path)
    driver.set_page_load_timeout(10)
    driver.implicitly_wait(5)
    driver.get(link)
    return driver


def get_beers(location):
    """
    Helper function for getting any beer location
    :param location, driver:
    :return:
    """
    if location == 'Bier30':
        return get_bier30()
    elif location == 'BitterCreek':
        return get_bittercreek()
    elif location == 'Fred Meyer - East Bench':
        return get_fredmeyer_eb()
    elif location == 'Old Chicago - Downtown':
        return get_oldchicago_dt()
    elif location == 'Brewers Haven':
        return get_brewershaven()
    elif location == 'Yard House':
        return get_yardhouse_boise()
    elif location == 'Payette':
        return get_payette()
    elif location == 'Boise Brewing':
        return get_boise_brewing()
    else:
        return []


def get_beers_from_elements(elements):
    """
    Takes in a standard scraping format we have been using to extract beers as a list of elements
    :param elements:
    :return: new encoded list of those beers
    """
    beers = []
    for element in elements:
        beer = element.text
        beer.encode('utf-8')
        if beer != '':
            beers.append(beer)
    return beers


def get_bier30():
    """
    Scrapes Bier 30's website and retreives a list of beers
    :return: list of Bier 30's beers
    """
    driver = None

    try:
        link = "http://api.menutabapp.com/restaurants/123608301077315/menus.htm"
        driver = get_driver(link)

        elements = driver.find_elements(By.CSS_SELECTOR, "div.container.paddingtop10.paddingbottom40")
        elements = elements[0].text
        beers = elements.split('\n')

        for beer in beers:
            beer.encode('utf-8')

        logging.debug('Bier 30 beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()

    return beers


def get_bittercreek():
    """
    Scrapes Bittercreek's website
    :return: list of bittercreek's current beers
    """
    driver = None

    try:
        link = "http://www.bcrfl.com/bittercreek/beers/"
        driver = get_driver(link)

        elements = driver.find_elements(By.CSS_SELECTOR, "p.beer-name")
        beers = get_beers_from_elements(elements)

        logging.debug('Bittercreek beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()

    return beers


def get_fredmeyer_eb():
    """
    Scrapes Fredmeyer East Bench's website
    :return: list of Fredmeyer's tap list
    """
    driver = None
    try:
        link = "http://growlers.fredmeyermedia.com/st/east-bench"
        driver = get_driver(link)

        elements = driver.find_elements(By.CSS_SELECTOR, "div.beername")
        beers = get_beers_from_elements(elements)

        logging.debug('FredMeyer-EB beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()

    return beers


def get_payette():
    """
    Scrapes Payette Brewing's website
    :return: list of beers on tap at Payette Brewing
    """
    driver = None
    try:
        link = "http://www.payettebrewing.com/about/"
        driver = get_driver(link)

        # this is to get around the age verification crap
        driver.find_element_by_id("age-gate-month").send_keys('01')
        driver.find_element_by_id("age-gate-day").send_keys('01')
        driver.find_element_by_id("age-gate-year").send_keys('1988')
        driver.find_element_by_xpath("//button[@type=\"submit\"]").click()

        # the page has 3 on-tap ul elements, none of which have an id
        on_tap_elements = driver.find_elements(By.CSS_SELECTOR, "ul.on-tap")
        elements = []
        for on_tap in on_tap_elements:
            elements = on_tap.find_elements_by_tag_name("li")
            # stop processing when we actually get elements, as only one of the ul's has children
            if len(elements) > 0:
                break

        beers = get_beers_from_elements(elements)

        logging.debug('Payette beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()

    return beers


def get_boise_brewing():
    """
    Scrapes Boise Brewings tap list
    :return: returns beers on tap in a list
    """
    driver = None
    try:
        link = "http://boisebrewing.com/tap-list/"
        driver = get_driver(link)

        # this is to get around the age verification crap
        driver.find_element_by_id("age-yes").click()
        driver.find_element_by_xpath("//button[@type=\"submit\"]").click()

        # the page has 3 on-tap ul elements, none of which have an id
        elements = driver.find_elements(By.CSS_SELECTOR, "p.beer-name")
        beers = get_beers_from_elements(elements)

        logging.debug('Boise Brewing beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()
    return beers


def get_oldchicago_dt():
    """
    Scrapes Old Chicago's downtown tap list
    :return: returns beers on tap in a list
    """
    driver = None
    try:
        link = "http://oldchicago.com/locations/downtown-boise"
        driver = get_driver(link)

        elements = driver.find_element(By.CSS_SELECTOR, "div.ReactTabs__TabPanel").text
        elements = elements.split('\n')

        #first element is the title
        elements = elements[1:]

        #this list has a subtitle with ABVs and we don't want those, this
        #I think is better than just blindly removing every other element
        beers = []
        for element in elements:
            element.encode('utf-8')
            if '%' not in element and '|' not in element:
                beers.append(element)

        logging.debug('Old Chicago beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()
    return beers


def get_brewershaven():
    """
    Scrapes Brewer's Haven tap list
    :return: returns beers on tap in a list
    """
    driver = None
    try:
        link = "http://www.brewershaven.com/pages/whats-on-tap"
        driver = get_driver(link)

        elements = driver.find_element(By.CSS_SELECTOR, "div.article.textile").text
        elements = elements.split('\n')

        #first element is garbage
        elements = elements[1:]

        #this one is tricky because there are few ids or classes,
        #but basically of there beers include a %ABV, so we'll use that for now
        beers = []
        for element in elements:
            element.encode('utf-8')
            if '%' in element:
                beers.append(element.strip())

        logging.debug('Brewers Haven beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()
    return beers

def get_yardhouse_boise():
    """
    Scrapes yardhouse in teh village
    :return:
    """
    driver = None
    try:
        link = "http://info.yardhouse.com/menu-listing/beer/?loc=8351"
        driver = get_driver(link)

        elements = driver.find_elements(By.CSS_SELECTOR, "div.menu_item_title")
        beers = get_beers_from_elements(elements)

        driver.quit()
        logging.debug('Yardhouse beers: {beers}'.format(beers=beers))
    except:
        e = sys.exc_info()[0]
        logging.error("Error: %s" % e)
        return []
    finally:
        driver.quit()
    return beers

scrape()
