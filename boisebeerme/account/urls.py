from django.conf.urls import url
from boisebeerme.account.views import *

urlpatterns = [
	url(r'^login', log_in, name='log_in'),
	url(r'^logout', log_out, name='log_out'),
	url(r'^signup', sign_up, name='sign_up'),
	url(r'^unsubscribe', unsubscribe, name='unsubscribe'),
	url(r'^resubscribe', resubscribe, name='resubscribe'),
	url(r'^update_preferences', update_preferences, name='update_preferences'),
	url(r'^home', home, name='home'),
]
