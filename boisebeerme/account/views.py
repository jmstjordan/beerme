from django.shortcuts import render
from .forms import SignUpForm, SubscriberForm
from django.contrib.auth import authenticate, login, logout
from boisebeerme.account.models import User, Subscriber
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

def log_in(request):
	if request.POST:
		user_name = request.POST['user_name']
		password = request.POST['user_password']
		user = authenticate(username=user_name, password=password)
		if user is not None:
			login(request, user)
			return redirect('/account/home/')
	messages.error(request, 'Login failed')
	return redirect('/home')

@login_required
def log_out(request):
	logout(request)
	return redirect('/home')

@login_required
def update_preferences(request):
	print('update pref data {data}'.format(data=request.POST))

	user = request.user

	if user is not None:
		subscriber = user.subscriber
		form = SubscriberForm(request.POST, instance=subscriber)
		print(form)
		form.save()
		messages.success(request, 'Preferences have been updated.')
		return redirect('/account/home/')
	return render(request, 'index.html')

@login_required
def resubscribe(request):
	user = User.objects.get(username=request.user)
	if user is not None:
		subscriber = user.subscriber
		if subscriber is not None:
			subscriber.is_subscriber = True
			subscriber.save()
			messages.success(request, 'Successfully resubscribed.')
			return redirect('/account/home/')
	return render(request, 'index.html')

@login_required
def unsubscribe(request):
	user = User.objects.get(username=request.user)
	if user is not None:
		subscriber = user.subscriber
		if subscriber is not None:
			subscriber.is_subscriber = False
			subscriber.save()
			messages.success(request, 'Successfully unsubscribed.')
			return redirect('/account/home/')
	return render(request, 'index.html')

@login_required
def home(request):
	if request.user is not None:
		return render(request, 'account.html', {'pref_form': SubscriberForm(instance=request.user.subscriber)})
	return render(request, 'index.html')


def sign_up(request):
	form = SignUpForm(request.POST)
	print('sign up post data {data}'.format(data=request.POST))

	return_values = {
		'user_name': '',
		'email': '',
		'total_users': User.objects.all().count() + 13
	}

	if form.is_valid() and request.POST:
		user_name = form.cleaned_data['user_name']
		password = form.cleaned_data['user_password']
		confirm_password = form.cleaned_data['confirm_password']
		email = form.cleaned_data['user_email']

		return_values['user_name'] = user_name
		return_values['email'] = email

		if confirm_password != password:
			messages.error(request, 'Passwords did not match.')
			return render(request, 'signup.html', {'return_values': return_values})

		# if the user already exists
		user = authenticate(username=user_name, password=password)
		if user is not None:
			messages.error(request, 'Username already exists.')
			return render(request, 'signup.html', {'return_values': return_values})

		try:
			existing_user = User.objects.get(username=user_name)
			messages.error(request, 'Username already exists.')
			return render(request, 'signup.html', {'return_values': return_values})
		except User.DoesNotExist:
			new_user = User.objects.create_user(user_name, email, password)
			new_user.save()

		if new_user is not None:
			subscriber = Subscriber(user=new_user)
			subscriber.save()
			login(request, new_user)
			return redirect('/account/home/')

		return render(request, 'signup.html', {'return_values': return_values})
	return render(request, 'signup.html', {'return_values': return_values})
