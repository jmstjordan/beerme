from django import forms
from boisebeerme.account.models import Subscriber


class LogInForm(forms.Form):
    user_name = forms.CharField(max_length=100)
    user_password = forms.CharField(max_length=100)


class SignUpForm(forms.Form):
    user_name = forms.CharField(max_length=100)
    user_email = forms.CharField(max_length=100)
    user_password = forms.CharField(max_length=100)
    confirm_password = forms.CharField(max_length=100)


class SubscriberForm(forms.ModelForm):

    class Meta:
        model = Subscriber
        exclude = ['user', 'is_subscriber', 'location_request']
