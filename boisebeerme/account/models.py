from django.db import models
from django.contrib.auth.models import User


class Subscriber(models.Model):
	"""
	The base model for our customer user, who really ends up being
	just a subscriber- we will add on to this model as time goes on,
	but this starts with allowing us to control the user's subscription
	preferences
	"""
	user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, unique=True)
	is_subscriber = models.BooleanField(default=True)

	Bier30 = models.BooleanField(default=True)
	BitterCreek = models.BooleanField(default=True)
	FredMeyerEB = models.BooleanField(default=True)
	Payette = models.BooleanField(default=True)
	BoiseBrewing = models.BooleanField(default=True)
	OldChicagoDT= models.BooleanField(default=True)
	BrewersHaven = models.BooleanField(default=True)
	YardHouse = models.BooleanField(default=True)

	location_request = models.CharField(max_length=100, null=True)
