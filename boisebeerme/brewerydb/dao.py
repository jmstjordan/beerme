import requests
import boisebeerme.brewerydb.apikey as key
import boisebeerme.brewerydb.brewery_ids as bids
import difflib


k = key.key
ROOT_URL = 'http://api.brewerydb.com/v2/'


def get_beers_from_brewery(brewery_id):
	"""
	Returns list of beers from breweryId
	:param brewery_id:
	:return:
	"""

	url = '{root_url}brewery/{brewery_id}/beers?key={apikey}'\
		.format(root_url=ROOT_URL, brewery_id=brewery_id, apikey=k)

	result = requests.get(url).json()

	try:
		if result['message'] != 'Request Successful':
			return None
	except KeyError:
		print("No beers found for brewery_id {brewery_id}".format(brewery_id=brewery_id))
		return None


	data = None
	try:
		data = result['data']
	except KeyError:
		print("Data not found for brewery_id {brewery_id}".format(brewery_id=brewery_id))
		return None

	return data


def get_other_brewery_ids(brewery_ids_to_names):
	"""
	Takes in a dictionary of brewery ids from bdb mapped to the name,
	appends on to this dictionary for our pull breweries command
	:param brewery_ids_to_names:
	:return:
	"""
	other_brewery_ids_to_names = bids.brewery_ids

	for brewery_id, brewery_name in other_brewery_ids_to_names.items():
		brewery_ids_to_names[brewery_id] = brewery_name

	return brewery_ids_to_names

def get_boise_brewery_ids_by_radius(radius=25):
	"""
	Returns json of all breweries with in passed in
	radius from idaho state capitol building. Default
	radius is 25 miles
	:param radius:
	:return:
	"""

	radius = str(radius)
	LAT = '43.611456'
	LONG = '-116.1993366'

	url = '{root_url}search/geo/point?lat={lat}&lng={long}&radius={radius}&key={apikey}'\
		.format(root_url=ROOT_URL, lat=LAT, long=LONG, radius=radius, apikey=k)

	result = requests.get(url).json()
	data = None
	try:
		# in this particular search, the key doesnt exist if there are no results
		data = result['data']
		if result['data'][0]['status'] != 'verified':
			return None
	except KeyError:
		return None


	brewery_ids_to_names = {}
	for brewery in data:
		try:
			if brewery['brewery']['status'] == 'verified':
				brewery_ids_to_names[brewery['brewery']['id']] = brewery['brewery']['name']
		except KeyError:
			pass

	return brewery_ids_to_names


def get_beer_json_by_id(beer_id):
	"""
	Simple getter here for beer_id
	:param beer_id:
	:return: beer result
	"""
	url = ROOT_URL + 'beer/' + beer_id + '?withBreweries=y?key=' + k
	return requests.get(url).json()


def get_brewery_json_by_id(beer_id):
	"""
	Simple helper to get brewery json by beer_id
	:param beer_id:
	:return:
	"""
	'http://api.brewerydb.com/v2/beer/kID4PM/breweries?key=378d02fc59dfc7cecc049375d7b3fc7e'
	url = ROOT_URL + 'beer/' + beer_id + '/breweries?key=' + k
	return requests.get(url).json()


def get_misc_beer_stats(beer_id):
	"""
	Return a dictionary that is associated with every beer that has a valid beer id

	:param beer_id:
	:return: tuple of misc stats
	"""

	print('Getting beer stats by beer id: {id}'.format(id=beer_id))
	dict_stats = {}

	dict_stats['bdb_id'] = beer_id

	result = get_beer_json_by_id(beer_id)
	if result['message'] == 'Request Successful':
		try:
			dict_stats['ibu'] = result['data']['ibu']
		except KeyError:
			pass
		try:
			dict_stats['abv'] = result['data']['abv']
		except KeyError:
			pass
		try:
			dict_stats['name'] = result['data']['name']
		except KeyError:
			pass
		try:
			dict_stats['description'] = result['data']['description']
		except KeyError:
			pass

	result = get_brewery_json_by_id(beer_id)
	if result['message'] == 'Request Successful':
		try:
			dict_stats['brewery'] = result['data'][0]['name']
		except KeyError:
			pass

	return dict_stats


def search_for_id(beer_name):
	"""
	Return BeerId associated with beername from BDB


	http://www.brewerydb.com/developers/docs-endpoint/search_index

	:param beer_name:
	:return: beerid from bdb, or 'NO_ID'
	"""

	print('Searching for beer name: {name}'.format(name=beer_name))

	url = ROOT_URL + 'search?q=' + beer_name + '&type=beer&key=' + k
	result = requests.get(url).json()

	beer_names_to_ids = {}
	beer_names = []

	# grabbing all the valid names and ids first
	# this looks ugly, building a prototype first and will update
	# later. oh man this is gross just want to get it working first
	if result['status'] == 'success':
		for item in result['data']:
			if item['status'] == 'verified':
				try:
					beer_names_to_ids[item['name']] = item['id']
				except KeyError:
					pass
				beer_names.append(item['name'])
	else:
		return 'NO_ID'

	# now we have a dict with the ids, so we don't have to iterate again
	# but now we need to iterate through our list and find the best match,
	# then subsequently grab that id
	print('\n\n\nBeerNamesToIds: {d}'.format(d=beer_names_to_ids))
	print('\n\n\nBeer_Names: {a}'.format(a=beer_names))

	best_matches = difflib.get_close_matches(beer_name, beer_names)
	print('\n\n\nBest matches: {matches}'.format(matches=best_matches))

	return beer_names_to_ids[best_matches[0]] if len(best_matches) > 0 else 'NO_ID'
