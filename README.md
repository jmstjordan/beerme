##Title
BoiseBeerMe
##Author
JM Jordan
##What am I looking at
This is a web application and beer scraper that crawls local Boise breweries
and tasting rooms for their weekly taplist. Basically, I got tired of searching
through all the local websites for what was on tap every Friday, so I wrote this
to consolidate that list.

I also wrote a web application in Django that will displays information on the local
taps, and allows users to subscribe to the locations that interest them.

The bulk of the scraper is located in scraper/BeerScraper.py

Most of the backend logic for the managing of the beer information is in api/management/commands
folder.

##How can I try this out?
Unfortunately, I took the server down, so you can't sign up anymore. I wish I took
screenshots, but I did not. You'll have to trust me that it looked awesome!

One day I will fire this back up, but in the mean time, I reflect on the the scraper
technology frequently for new projects, and all of my future projects build on this one (this
was my first Django/AngularJS project)
##Technologies
This app utilizes Selenium for its scraping because of the javascript hurdles many
local websites had me jumping over. It uses Django as the web framework, AngularJS
(my first attempt at it), Bootstrap for layout, and was once hosted on DigitalOcean.
I utilized cron jobs which executed Django commands for jobs. I also utilized BreweryDB
to match my beers which a global database so I could get more information on each beer.
##What's next?
This project is retired for now. It was a lot of fun!
